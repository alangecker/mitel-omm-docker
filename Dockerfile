FROM centos:7

RUN yum -y update
RUN yum -y install unzip telnet wget sysvinit-tools \
        zlib.i686 libgcc.i686 libstdc++.i686 \
        net-tools psmisc system-config-keyboard

COPY ./sip-dect.zip /sip-dect.zip
RUN unzip /sip-dect.zip -d /setup && \
    rm /sip-dect.zip && \
    /setup/SIP-DECT.bin && \
    rm -rf /setup

RUN echo "OMM_CONFIG_FILE=/conf/omm_conf.txt" > /etc/sysconfig/SIP-DECT

COPY ./entrypoint.sh /entrypoint.sh 

CMD /entrypoint.sh

