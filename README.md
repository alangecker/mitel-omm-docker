# Mitel OpenMobility Manager in docker
Current Version: 8.1SP2

Take care: Latest version supporting 2G devices (RFP32, ...) is 8.0SP1-EF04 !

## requirements
- docker
- docker-compose
- git

## install
```bash
git clone https://gitlab.com/alangecker/mitel-omm-docker.git

cd mitel-omm-docker
```

## provide OMM software package
* download from somewhere (use search engine of your choice and search the version string you are looking for)
* unzip first level
* rename zip file in there to `sip-dect.zip` and put it into this repositories folder
* optionally put the firmware files into the serving folder of your TFTP server

## use
```bash
docker-compose up
```

URL: https://localhost:8443/ (ignore certificate errors)

Default user: `omm:omm`

## Deploy
A deployment needs additionally a DHCP server to configure the RFP as well as a TFTP server to provide the firmware.

* DHCP network boot: put IP address of tftp server into `server` and the name of the firmware (e.g. `iprfp2G.tftp`) into the bios file / boot file field
* DHCP Option 224: Text `OpenMobilitySIP-DECT`
* DHCP Option 43: hex / string  `11:02:00:01:0a:04:C0:A8:01:05` (16 2 byte 1 lang DE; 10 4 byte IP of OMM 192.168.1.5)

some implementations might need an additional `FF` at the end of the hex string.

Debian:
```
sudo apt install tftpd
cp iprfp2G.tftp /srv/tftp
```

## Configure
I found it hard to add devices / bind devices to SIP accounts in the web interface.
It is much easier to use the JAVA tool you get on https://localhost:8443/OMP.jar.
Download it, start with `java -jar OMP.jar` (if you are lucky, it might also work by clicking on the OMP link top right in web interface) and login using the same credentials/IP as for the OMM.
Then, connect your RFPs, create a DECT site (you might need to request a PARK code) and configure a DECT pin. Finally, click "Create on subscription" and "subscribe" in the bottom.
Now, connect with your  DECT phone. This allows you to retrieve the IPEI in the device UI. You could write this down and use it to add a device with a fixed registraton in the web interface; or you activate the login feature code (feature codes: \*1, login: 1) and set a Login ID on the SIP account and then use `*11 <LoginID>` e.g. `*111000` on your phone. You are then asked the the PIN you gave to this login ID. This registers your device dynamically to this SIP account. You could now use the OMP java tool, go to the devices overview and change the link type to static to permanently fix this device to this SIP account :-)
Easy, hm?

Maybe someone can have a look at what the people from Eventphone did: https://github.com/eventphone/ and write a simple, nice CLI (or web ui?) to be able to actually use the omm nicely...
