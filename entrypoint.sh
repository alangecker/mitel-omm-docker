#!/bin/bash

rm /opt/SIP-DECT/*.log 2>/dev/null || true

# touch /config/omm_conf.txt

# start SIP-DECT
/opt/SIP-DECT/bin/SIP-DECT.sh



# tail log file
OMM_PID_FILE="/var/run/SIP-DECT.pid"
PID=$(cat $OMM_PID_FILE)
tail -f -n 1000 --pid=$PID \
    /var/log/SIP-DECT.log \
    /opt/SIP-DECT/omm_trace_*.log \
    /opt/SIP-DECT/ics_trace_*.log